<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Seeder;

class Pages extends Seeder
{
    use SoftDeletes;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'title' => 'About',
            'content' => '',
            'slug' => 'about',
        ]);

        DB::table('pages')->insert([
            'title' => 'Contact',
            'content' => '',
            'slug' => 'contact',
        ]);
    }
}
