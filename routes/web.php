<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact', 'ContactController@create');


Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('menu/parents', 'Admin\MenuItemController@getParents');
    Route::resource('menu-items', 'Admin\MenuItemController');
    Route::resource('pages', 'Admin\PageController');
});
Route::resource('admin', 'Admin\IndexController')->middleware('auth');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{slug?}', 'PageController@show')->where('slug', '[a-zA-Z-_0-9]+');
//Route::get('/', 'PageController@index');

//Route::resource('/api/menu', 'MenuController');
Route::get('/api/menu/{slug}', 'MenuController@show');
