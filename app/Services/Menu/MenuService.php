<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-03-25
 * Time: 08:39
 */

namespace App\Services\Menu;

use App\Menu;

class MenuService
{
    private $model;

    /**
     * MenuService constructor.
     */
    public function __construct() {
        $this->model = new Menu;
    }

    /**
     * @param $request
     */
    public function make($request) {
        $this->model->create($request);
    }

    /**
     * @param $request
     * @param int $id
     */
    public function update($request, int $id) {
        $model = $this->model->where('id', $id)->firstOrFail();
        $model->update($request);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function delete(int $id) {
        $this->model->delete($id);
    }
}