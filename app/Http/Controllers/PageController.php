<?php

namespace App\Http\Controllers;

use App\Page;
use App\Repositories\Page\Contracts\PageRepositoryInterface;
use Illuminate\Http\Request;

class PageController extends Controller
{
    private $pageRepository;

    public function __construct(PageRepositoryInterface $pageRepository) {
        $this->pageRepository = $pageRepository;
    }

    public function index() {
        return view('generic_pages.index');
    }

    /**
     * @param Request $request
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, string $slug = '') {
        return view('generic_pages.show', ['page' => $this->pageRepository->findPageBySlug($slug)]);
    }
}
