<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-04-18
 * Time: 21:18
 */

namespace App\Http\Controllers\Admin;


use App\Http\Requests\PageRequest;
use App\Repositories\Page\Contracts\PageRepositoryInterface;

class PageController
{
    private $pageRepository;

    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.pages.index', ['pages' => $this->pageRepository->getAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $page = $this->pageRepository->create($request->validated());
        return redirect()->route('pages.edit', [$page->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id) {
        return view('admin.pages.show', ['page' => $this->pageRepository->findById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id) {
        return view('admin.pages.edit', ['page' => $this->pageRepository->findById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PageRequest $request
     * @param  int $id
     * @return void
     */
    public function update(PageRequest $request, int $id) {
        $this->pageRepository->update($id, $request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->pageRepository->delete($id);
        return redirect()->route('pages.index');
    }
}