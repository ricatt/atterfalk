<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Repositories\Menu\Contracts\MenuRepositoryInterface;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    private $menuRepository;

    public function __construct(MenuRepositoryInterface $menuRepository) {
        $this->menuRepository = $menuRepository;
    }

    public function index() {
    }

    public function show($param) {
        return response()->json($this->menuRepository->getByTitle($param));
    }
}
