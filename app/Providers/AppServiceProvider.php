<?php

namespace App\Providers;

use App\Repositories\Menu\Contracts\MenuRepositoryInterface;
use App\Repositories\Menu\MenuRepository;
use App\Repositories\Page\Contracts\PageRepositoryInterface;
use App\Repositories\Page\PageRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(MenuRepositoryInterface::class, MenuRepository::class);
        app()->bind(PageRepositoryInterface::class, PageRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
