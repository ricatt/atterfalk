<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $guarded = [];

    public function parent() {
        return $this->belongsTo(MenuItem::class, 'parent_id');
    }
}
