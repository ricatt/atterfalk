<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-04-11
 * Time: 08:42
 */

namespace App\Repositories\Menu\Contracts;


interface MenuRepositoryInterface
{

    public function getAll();

    public function getById(int $id);
}