<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-04-11
 * Time: 08:42
 */

namespace App\Repositories\Menu;


use App\Menu;
use App\MenuItem;
use App\Repositories\Menu\Contracts\MenuRepositoryInterface;

class MenuRepository implements MenuRepositoryInterface
{
    public function getById(int $id) {
        return Menu::with('menuItems')->where('id', $id)->first();
    }

    public function getAll() {
        return Menu::with('menuItems')->get();
    }

    public function getBySlug(string $slug) {
        return Menu::with('menuItems')->where('slug', $slug)->first();
    }

    /**
     * @param string $title
     * @return Menu|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByTitle(string $title) {
        return Menu::with('menuItems')->where('title', $title)->first();
    }

    /**
     * @param array $item
     * @return mixed
     */
    public function createMenuItem(array $item) {
        $parentId = $item['parent'];
        $item['menu_id'] = 1;
        $item['extra_attr'] = (string) $item['extra_attr'];
        unset($item['parent']);
        $menu = MenuItem::create($item);
        return $menu;
    }
}