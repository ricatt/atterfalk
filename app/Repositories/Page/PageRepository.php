<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-04-14
 * Time: 11:01
 */

namespace App\Repositories\Page;

use App\Page;
use App\Repositories\Page\Contracts\PageRepositoryInterface;
use Illuminate\Support\Str;

class PageRepository implements PageRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAll() {
        return Page::get();
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function findPageBySlug(string $slug = '') {
        if ('/' === $slug) {
            $slug = '';
        }
        return Page::where('slug', $slug)->firstOrFail();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id) {
        return Page::where('id', $id)->firstOrFail();
    }

    public function create(array $page) {
        $page['slug'] = Str::slug($page['title']);
        return Page::create($page);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data) {
        return Page::where('id', $id)->update($data);
    }

    public function delete(int $id) {
        return Page::destroy($id);
    }
}