<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-04-14
 * Time: 11:01
 */

namespace App\Repositories\Page\Contracts;


interface PageRepositoryInterface
{

    public function findById(int $id);

    public function update(int $id, array $validated);

    public function delete(int $id);
}