<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];

    public function menuItems() {
        return $this->hasMany(MenuItem::class);
    }
}
