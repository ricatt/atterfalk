<html>
<head>
    <title>{{ config('app.name') }} @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}" media="screen" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div id="app">
    @include('partial.admin-topnav')
    <div class="container-fluid">
        <div class="row">
            @include('partial.admin-navbar')
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                @yield('content')
            </main>
        </div>
    </div>
</div>
<script src="{{asset('js/admin.js')}}"></script>
@yield('javascript')
</body>
</html>
