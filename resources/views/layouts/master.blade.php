<html>
    <head>
        <title>{{ config('app.name') }} @yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" media="screen" />
    </head>
    <body>
        <div id="app">
            <top-nav></top-nav>
            <main class="container">
                @yield('content')
            </main>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
        @yield('javascript')
    </body>
</html>
