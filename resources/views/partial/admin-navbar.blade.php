<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="/admin">
                    {{--<span data-feather="home"></span>--}}
                    <span class="fa fa-dashboard"></span>
                    Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="/admin/menu-items">
                    <span class="fa fa-bars"></span>
                    Menu <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="/admin/pages">
                    <span class="fa fa-file"></span>
                    Pages <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>
    </div>
</nav>